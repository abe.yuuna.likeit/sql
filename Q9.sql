use sqltest;

SELECT category_name,SUM(item_price) AS total_price FROM item
 LEFT OUTER JOIN item_category on item.category_id = item_category.category_id
 GROUP BY category_name ORDER BY total_price DESC;
 
